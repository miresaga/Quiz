### Descripción:
Este **proyecto** se basa en una serie de _preguntas_ con 4 respuestas cada una.
<br>
Si aciertas o fallas se mostrará un mensaje conforme tu resultado.

### Requisitos:

1. Tener un ordenador con un navegador como **Google**.
<br>
2. Tener una cuenta en _Gitlab_.
<br>
3. Minimo 4gb de RAM

### Autores:
miresaga, nhelo34, viictorpg

