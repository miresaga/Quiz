﻿using System.Collections.Generic;

namespace Quiz.Models
{
    public class PreguntaModel
    {
        private string pregunta;
        private List<string> respuestasList = new List<string>();

        public PreguntaModel(string pregunta, List<string> respuestasList)
        {
            this.pregunta = pregunta;
            this.respuestasList = respuestasList;
        }
    }
}