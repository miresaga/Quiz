﻿using System.Collections.Generic;

namespace Quiz.Models
{
    public class QuizModel
    {
        private List<PreguntaModel> quiz = new List<PreguntaModel>(3);

        public QuizModel()
        {
            this.quiz.Add(new PreguntaModel("¿Como murió Hitler?", new List<string>() {"Lo mató un alien", "Se suicidó", "Lo mató un soldado ruso"}));
            this.quiz.Add(new PreguntaModel("¿Quien ganó el mundial de futbol 2018?", new List<string>(){ "España", "Macedonia", "Los chinos" }));
            this.quiz.Add(new PreguntaModel("¿Cual es el ingrediente secreto de la cangreburger?", new List<string>() { "Amor", "Nada", "Cangrejo" }));
        }

        public List<PreguntaModel> GetQuiz()
        {
            return this.quiz;
        }
    }
}